(include "globals.clj")
(def email-c (mailer {:host "smtp.gmail.com"
                    :port 587
                    :user "riemannuser@gmail.com"
                    :pass "riemannuser123"
                    :tls "true"
                    :subject (fn [events](apply str "Cpu Threshold reached on Host: " (get-in (first events) [:host])))
                    :body (fn [events]
                        (str "Metric value:" (get-in (first events) [:metric])))
                    :from "riemannuser@gmail.com"}))
(streams
  (by [:host]
    (where (and (host "54.88.127.179") (service "cpu/percent-active"))
           (where (> metric cpu_threshold)
              (rollup 1 1800 (email-c recepient))))))
