(use '[clojure.java.shell :only [sh]])
(require '[riemann.influxdb :as influxdb])
(def influx-put (influxdb/influxdb-9 {
                        :username "no_name"
                        :password "no_password"
                        :scheme "http"
                        :host "54.88.179.82"
                        :port 8086
                        :db "db_name_3"
			:series #(str (:host %) "." (:service %))}))
(streams
  (where (and (tag "Collectd info")
	      (tag "collectdinfo2"))
	(fn [event]
		(influx-put event)
		)))
;(def query_command (sh "curl" "-G" "'http://54.88.179.82:8086/query?pretty=true'" "--data-urlencode" ""db=mydb"" "--data-urlencode" ""q=SELECT value FROM cpu_load_short WHERE region='us-west'""))
;(println query_command)
