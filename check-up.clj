(include "globals.clj")
(use '[clojure.java.shell :only [sh]])
(def ch-w (atom ""))
(def stat (atom 1))
(def ch-subj (atom ""))
(def colstat (atom 100000))
(def email-ch (mailer {:host "smtp.gmail.com"
                    :port 587
                    :user "riemannuser@gmail.com"
                    :pass "riemannuser123"
                    :tls "true"
                    :subject (fn [events](str @ch-subj))
                    :body (fn [eve](str @ch-w))
                    :from "riemannuser4@gmail.com"}))
(defn col-down[e]
	(reset! ch-subj (str "Collectd is down ::" (get e :host)))
	((email-ch recepient) e)
)
(defn host-down[e]
	(reset! ch-subj (str "Host is down ::" (get e :host)))
	((email-ch recepient) e)
)
(defn expired-stream [& children]
  (fn [e] (let [new-event (assoc e :stat stat)]
	(def rt (sh "ansible-playbook" "--private-key" "/home/ec2-user/ansib/keys/54.88.127.179.pem" "-i" "/home/ec2-user/ansib/inventory-54.88.127.179" "/home/ec2-user/ansib/playbook/test3.yml"))
	(def qw (get rt :out))
	(reset! colstat (nth (re-find #"(\"msg\"): (\"(\d+)\")" qw) 3))
	(reset! stat (get rt :exit))
	(if (and (= @stat 0) (= @colstat "1")) (prn :yup)
			(prn :nope))
	(if (and (= @stat 0) (= @colstat "1")) (col-down e)
			(prn :some))
	(if (= @stat 3) (host-down e)
			(prn :host-is-up))
	(reset! ch-w (get rt :out))
	(call-rescue new-event children)
)))
(streams 
	(where (state "expired")
		 (expired-stream prn)
))

