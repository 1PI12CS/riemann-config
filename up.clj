(include "globals.clj")
(def r-subj (atom ""))
(def r-w (atom ""))
(def email-up (mailer {:host "smtp.gmail.com"
                    :port 587
                    :user "riemannuser@gmail.com"
                    :pass "riemannuser123"
                    :tls "true"
                    :subject (fn [events](str @r-subj))
                    :body (fn [eve](str @r-w))
                    :from "riemannuser@gmail.com"}))
(defn uptime-stream [& children]
  (fn [e] (let [new-event (assoc e :uptime "system rebooted")]
		(reset! r-subj (str " 112 Restarted the host ::" (get e :host)))
	        (reset! r-w (str "Uptime is found to be ::" (get e :metric)))
	(call-rescue new-event children)))
)
(streams
        (where (and  (service "uptime/uptime")
                     (host "54.88.127.179")
		     (< 0 metric uptime_host))
       		     (uptime-stream prn)
		     (rollup 1 60 (email-up recepient))
        )
)
