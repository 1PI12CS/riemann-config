(include "globals.clj")
(include "influx.clj")
(include "up.clj")
(include "check-up.clj")
(include "cpu.clj")
(use '[clojure.java.shell :only [sh]])
(logging/init {:file "riemann.log"})
(def r-w (atom ""))
(def r-subj (atom ""))
(let [host "0.0.0.0"]
  (tcp-server {:host host})
  (udp-server {:host host})
  (ws-server  {:host host}))
(periodically-expire 60 {:keep-keys [:host :service :metric]})
(let [index (index)]
  (streams
   (default :ttl 60
      index
      (expired
        (fn [event] (info "expired" event))))))
(def email (mailer {:host "smtp.gmail.com"
                    :port 587
                    :user "riemannuser@gmail.com"
                    :pass "riemannuser123"
                    :tls "true"
                    :subject (fn [events](str @r-subj))   
                    :body (fn [eve](str @r-w))
                    :from "riemannuser@gmail.com"}))
(defn exceed-stream-file [& children]
  (fn [e] (let [new-event (assoc e :status "warning-limit-file")]
        	(reset! r-subj (str "Warning disk filling  up on ::" (get e :host)))
		(reset! r-w "Please take a look ")
		(call-rescue new-event children))
))
(streams
	(where (and  (service "df-root/percent_bytes-used")
	    	     (host "54.88.127.179")
		     (< df_warning_lb metric df_warning_ub))
		     (exceed-stream-file prn)
		     (rollup 1 1800
     		     (email recepient))
))
(defn delete-file-stream [& children]
  (fn [e] (let [new-event (assoc e :status "deleting-exceed-file")]
    		(def rt (sh "ansible-playbook" "--private-key" "/home/ec2-user/ansib/keys/54.88.127.179.pem" "-s" "-i" "/home/ec2-user/ansib/inventory-54.88.127.179" "/home/ec2-user/ansib/playbook/test1.yml"))
		(reset! r-subj (str "I freed up some space on ::" (get e :host)))
		(reset! r-w "I deleted files under samp")
		(call-rescue new-event children))
))
(streams
	(where (and  (service "df-root/percent_bytes-used")
	    	     (host "54.88.127.179")
		     (> metric df_warning_ub))
		     (delete-file-stream prn)
			(rollup 1 1800
			(email recepient))
))
(comment
(defn delete-file-stream [& children]
  (fn [e] (let [new-event (assoc e :status "deleting-exceed-file")]
		(def inv (str "/home/ec2-user/ansib/inventory-" (get e :host)))
		(def keyval (str "/home/ec2-user/ansib/keys/" (get e :host) ".pem"))
    		(def rt (sh "ansible-playbook" "--private-key" keyval "-s" "-i" inv "/home/ec2-user/ansib/playbook/test1.yml"))
                (prn rt)
		(def r-subj (str "I freed up some space on ::" (get e :host)))
		(def r-w "I deleted files under samp")
		(reset! r-subj (str "I freed up some space on ::" (get e :host)))
		(reset! r-w "I deleted files under samp")
		(call-rescue new-event children))
))
(streams
	(by :host
	(where (and  (service "df-root/percent_bytes-used")
		     (< df_warning_lb metric df_warning_ub))
		     (exceed-stream-file prn
		     (rollup 1 1800
     		     (email "varunsk999@gmail.com")))
	)
	(where (and  (service "df-root/percent_bytes-used")
                     (> metric df_warning_ub))
                     (delete-file-stream prn)
                     (email "varunsk999@gmail.com")
	)
))
)
